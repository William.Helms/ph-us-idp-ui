This repository should be used for building and deploying custom user interfaces for prompts rendered and served by the Yum ECommerce Identity Provider (IDP).

For more information see YumKB's articles on Storefront Authentication:
https://yumkb.yumconnect.dev/docs/Guides/Commerce/Auth/Storefront-Authentication
https://yumkb.yumconnect.dev/docs/Guides/Commerce/Auth/Auth-Flow-Customization

## Setup
The section will help you run a piece of the ECommerce Platform locally so that you can see your UI changes in real time. 

As a prerequisite, make sure you have the following installed:
* **Node & NPM** (optionally with a Node Version Manager)
* **Docker** (optionally with docker-desktop)

1. Create an `.npmrc` file with the following contents:
```
; docs: https://gitlab.yum.com/help/user/packages/npm_registry/index.md
@yc-commerce:registry=https://gitlab.yum.com/api/v4/packages/npm/
@yum-shared:registry=https://gitlab.yum.com/api/v4/packages/npm/
//gitlab.yum.com/api/v4/packages/npm/:_authToken={GITLAB_TOKEN}
//gitlab.yum.com/api/v4/projects/24/packages/npm/:_authToken={GITLAB_TOKEN}
//gitlab.yum.com/api/v4/projects/25/packages/npm/:_authToken={GITLAB_TOKEN}
//gitlab.yum.com/api/v4/projects/123/packages/npm/:_authToken={GITLAB_TOKEN}
```
This will allow `npm` to download packages from Yum's repositories. If your team has not received a Gitlab token, ask for one on the `#commerce-auth-and-customer` slack channel, or reach out to their product manager.

2. **If you are not logged into docker**
```
docker login https://registry.gitlab.yum.com -u {your user id} -p <gitlab_access_token>
```

3. Make sure that the `client-definition.json` file includes a client with sensible configuration. Most likely the default values there will be fine. Please refer to YumKB articles above if you are not familiar with client configuration.

3. Run
```console
npm install
```
You may receive a 401 when trying to npm install
```
401 Unauthorized - GET https://gitlab.yum.com/api/v4/projects/746/packages/npm/@yc-commerce/sample-idp-client/-/@yc-commerce/sample-idp-client-1.3.0.tgz
```
Note the project number in that url, add add it to your `.npmrc` file:
```
//gitlab.yum.com/api/v4/projects/746/packages/npm/:_authToken={GITLAB_TOKEN}
```

4. Run
```
npm run start
```
This will start a portion of the Yum ECommerce platform on your machine using docker.

5. If you have not started implementation of your client, run
```
npm run sample-client
```
This will start a sample IDP client in which you can type out your client ID and redirect url, and click "login" in order to see your IDP UI.

## Troubleshooting
This section is for known issues when trying to run the project. 

* If you get an error after running `npm run start` stating port 7000 is already in use, it may be your Macs Airplay receiver settings using the port.  You can disable Airplay receiver in your system settings to release the port. Link for reference: https://developer.apple.com/forums/thread/682332.


## Structure of this Repository
* `client-definition` - contains a JSON file with a client definition that you can use *only for local testing*. This client definition is injected into the running Yum Identity Provider when you use the Yum Platform locally.
* `ENTER_ORG_ID_HERE` - contains a JSON file with configurations that you can use *only for local testing*. These configurations are injected into the running Yum Identity Provider when you use the Yum Platform locally, and they override the default ones it comes with.
* `views` - the folder containing nested folders for handlebars files
  * `prompts` - handlebars files for predefined screens.
    * `locales` - optional. [I18next](https://www.i18next.com/) localization files to be used with handlebars `t` function. Filenames must only contain letters.
  * `layouts` - handlebars layout file. At the moment, there's only 1.
  * `partials` - optional. handlebars files containing reused elements.

The rest includes specifics for our deployment pipeline, which will deploy your files directly to a running instance of the IDP service.